data "aws_api_gateway_rest_api" "global-api-gw" {
  name = "smiley-global-apigw"
}

data "aws_api_gateway_resource" "api-resource" {
  rest_api_id = data.aws_api_gateway_rest_api.global-api-gw.id
  path        = "/api"
}

resource "aws_api_gateway_deployment" "stage-deploy" {
  rest_api_id = data.aws_api_gateway_rest_api.global-api-gw.id
}

resource "aws_api_gateway_stage" "stage" {
  deployment_id = aws_api_gateway_deployment.stage-deploy.id
  rest_api_id   = data.aws_api_gateway_rest_api.global-api-gw.id
  stage_name    = "stage-${var.environment}"
  variables = {
    feelingSubmitLambdaArn = aws_lambda_function.submit-feeling-lambda.function_name
  }
}

resource "aws_lambda_permission" "apigw-lambda-permission" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.submit-feeling-lambda.function_name
  principal     = "apigateway.amazonaws.com"


  source_arn = "${data.aws_api_gateway_rest_api.global-api-gw.execution_arn}/*"
}
