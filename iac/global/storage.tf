resource "aws_s3_bucket" "smiley-bucket-artifact" {
  bucket = "smiley-artifact-global"
  tags = local.common-tags
}

resource "aws_s3_bucket_acl" "smiley-bucket-artifact-acl" {
  bucket = aws_s3_bucket.smiley-bucket-artifact.id
  acl    = "private"
}