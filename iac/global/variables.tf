variable "domain" {
    type = string
    description = "Domain name of website"
}